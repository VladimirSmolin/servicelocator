﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Locator 
{
    private Locator() { }

    private readonly Dictionary<string, IController> controllers = new Dictionary<string, IController>();
    public static Locator locatorInstance { get; private set; }

    public static void Init() 
    {
        locatorInstance = new Locator();
    }


    //Get
    public T Get<T>() where T : IController 
    {
        string key = typeof(T).Name;

        if (!controllers.ContainsKey(key)) 
        {
            Debug.LogError($"{key} not registrated with {GetType().Name}");
            throw new InvalidOperationException();
        }

        return (T)controllers[key];
    }

    public void RegisterController<T>(T controller) where T: IController
    {
        string key = typeof(T).Name;

        if (controllers.ContainsKey(key)) 
        {
            Debug.LogError($"Attempted to register service {key} whitch is already registrated.");
            return;
        }

        controllers.Add(key, controller);
    }

    //Unregister
    public void Unregister<T>() where T : IController 
    {
        string key = typeof(T).Name;

        if (!controllers.ContainsKey(key)) 
        {
            Debug.LogError($"{key} is null");
            return;
        }

        controllers.Remove(key);
    }
}
